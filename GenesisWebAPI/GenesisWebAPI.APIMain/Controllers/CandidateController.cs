﻿using GenesisWebApi.DTO.RequestDTOs;
using GenesisWebAPI.DALLayer;
using System;
using System.Net;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;
using MySql.Data.MySqlClient;
using System.Data;

namespace GenesisWebAPI.APIMain.Controllers
{
    public class CandidateController : ApiController
    {          

        [HttpGet]
        [BasicAuthentication]
        public IHttpActionResult Get(int id, string organizationName)
        {
            HttpContext httpContext = HttpContext.Current;
            string authHeader = httpContext.Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            var username = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);
            string organizationId = new DataAccessLayer().GetOrganizationId(organizationName);
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();
            if (_dataAccessLayer.CheckValidUserFromOrganization(Convert.ToInt32(organizationId), username, password))
            {
                CandidateRequestDTO data = new DataAccessLayer().GetCandidateData(id);
                return Ok(data);
            }
            else
            {
                return BadRequest();
            }
        }

        public IHttpActionResult GetAllCandidates(string organizationId)
        {
            HttpContext httpContext = HttpContext.Current;
            string authHeader = httpContext.Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            var username = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);

            DataAccessLayer _dataAccessLayer = new DataAccessLayer();

            if (_dataAccessLayer.CheckValidUserFromOrganization(Convert.ToInt32(organizationId), username, password))
            {
                CandidateRequestDTO[] data = _dataAccessLayer.GetAllCandidatesOfOrganization(organizationId);
                return Ok(data);
            }
            else
            {
                return BadRequest();
            }
            
        }

        [HttpGet]
        [BasicAuthentication]
        public IHttpActionResult CandidateStatus(string organizationName, int id)
        {
            HttpContext httpContext = HttpContext.Current;
            string authHeader = httpContext.Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            var username = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);
            string organizationId = new DataAccessLayer().GetOrganizationId(organizationName);
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();
            if (_dataAccessLayer.CheckValidUserFromOrganization(Convert.ToInt32(organizationId), username, password))
            {
                string _candidateStatus = _dataAccessLayer.GetCandidateStatus(id, Convert.ToInt32(organizationId));
                string _color = GetCandidateStatusColor(_candidateStatus);
                if(_candidateStatus == "Insufficiency Raised")
                {
                    string _InSuffRaisedDate = String.Empty; string _InSuffClearedDate = "";
                    _InSuffRaisedDate = _dataAccessLayer.GetInsuffDates(id, out _InSuffClearedDate);
                    return Content(HttpStatusCode.OK, JObject.Parse("{success:true,CandidateID:" + id + ",Status:\"" + _candidateStatus + "\",Color:\"" + _color + "\",InSufficiencyRaisedDate:\""+_InSuffRaisedDate+"\",InSufficiencyClearDate:\""+_InSuffClearedDate+"\"}"));
                }
                else
                {
                    return Content(HttpStatusCode.OK, JObject.Parse("{success:true,CandidateID:" + id + ",Status:\"" + _candidateStatus + "\",Color:\"" + _color + "\"}"));
                }               
            }
            else
            {
                return BadRequest();
            }
        }
      

        private string GetCandidateStatusColor(string _candidateStatus)
        {
            string color = String.Empty;
            switch (_candidateStatus)
            {
                case "Major Discrepancy":
                    color = "Red";
                    break;
                case "Minor Discrepancy":
                    color = "Orange";
                    break;
                case "Clear":
                case "Interim Report Shared":
                    color = "Green";
                    break;
                case "Insufficient Raised":
                    color = "Pink";
                    break;
                case "In Process":
                    color = "Light Blue";
                    break;
                case "Inaccessible For Verification":
                case "Unable To Verify":
                case "Stop Check":
                case "Not Submitted":
                    color = "Yellow";
                    break;
                default: break;
            }
            return color;
        }

        [HttpGet]
        [BasicAuthentication]
        public HttpResponseMessage GetReport(string organizationName,int id)
        {
            HttpContext httpContext = HttpContext.Current;
            string authHeader = httpContext.Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            var username = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);
            string organizationId = new DataAccessLayer().GetOrganizationId(organizationName);
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();
            HttpResponseMessage result = null;
            if (_dataAccessLayer.CheckValidUserFromOrganization(Convert.ToInt32(organizationId), username, password))
            {
                
                DataAccessLayer _dataLayer = new DataAccessLayer();
                string _reportFilename = String.Empty;
                var fileBytes = _dataLayer.GetFinalReport(id, out _reportFilename);
                if (fileBytes != null)
                {
                    var fileMemStream = new MemoryStream(fileBytes);
                    result = new HttpResponseMessage(HttpStatusCode.OK);
                    result.Content = new StreamContent(fileMemStream);
                    var headers = result.Content.Headers;
                    headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    headers.ContentDisposition.FileName = _reportFilename;
                    headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    headers.ContentLength = fileMemStream.Length;
                }
                else
                {
                    return new HttpResponseMessage()
                    {
                        Content =new StringContent("No Report found for this candidate.")
                    };
                }
            }
            else
            {
                result = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            return result;
        }


        [HttpPost]
        public IHttpActionResult Post([FromBody] CandidateRequestDTO candidate)
        {
            HttpContext httpContext = HttpContext.Current;
            string authHeader = httpContext.Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            var username = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);
            string organizationId = candidate.OrganizationId;
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();

            if (_dataAccessLayer.CheckValidUserFromOrganization(Convert.ToInt32(organizationId), username, password))
            {
                int i = _dataAccessLayer.SubmitCandidateDetails(candidate);
                return Content(HttpStatusCode.Created, JObject.Parse("{success:true, candidateId:" + i + "}"));
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpPut]        
        public IHttpActionResult UpdateCandidate([FromBody]CandidateRequestDTO candidate)
        {
            HttpContext httpContext = HttpContext.Current;
            string authHeader = httpContext.Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            var username = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);
            string organizationId = candidate.OrganizationId;
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();
            if (_dataAccessLayer.CheckValidUserFromOrganization(Convert.ToInt32(organizationId), username, password))
            {
                if (candidate.CandidateId == null)
                {
                    Content(HttpStatusCode.BadRequest, JObject.Parse("{success:false, Message:\"Candidate id is not present in request\"}"));
                }
                int i = new DataAccessLayer().SubmitCandidateDetails(candidate);
                return Content(HttpStatusCode.OK, JObject.Parse("{success:true,Message:\"Candidate id " + Convert.ToString(candidate.CandidateId) + " updated successfully\"}"));
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id, string organizationName)
        {
            HttpContext httpContext = HttpContext.Current;
            string authHeader = httpContext.Request.Headers["Authorization"];
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

            int seperatorIndex = usernamePassword.IndexOf(':');

            var username = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();
            string organizationId = _dataAccessLayer.GetOrganizationId(organizationName);

            if (_dataAccessLayer.CheckValidUserFromOrganization(Convert.ToInt32(organizationId), username, password))
            {
               int i =  _dataAccessLayer.DeleteCandidateRecord(id, organizationId);
                if (i == 1)
                {
                    return Content(HttpStatusCode.OK, JObject.Parse("{success:true,Message:\"Candidate id " + Convert.ToString(id) + " deleted successfully\"}"));
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
