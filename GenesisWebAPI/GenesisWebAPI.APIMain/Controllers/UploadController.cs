﻿using GenesisWebApi.DTO.ResponseDTOs;
using GenesisWebApi.DTO.RequestDTOs;
using GenesisWebAPI.DALLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net;
using System.Web.Http.Results;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using MySql.Data.MySqlClient;

namespace GenesisWebAPI.APIMain.Controllers
{
    public class UploadController : ApiController
    {  
        [HttpPut]
        public async Task<HttpResponseMessage> UpdateDocuments()
        {
            string root = HttpContext.Current.Server.MapPath("~/Images");
            var provider = new MultipartFormDataStreamProvider(root);

            var result = await Request.Content.ReadAsMultipartAsync(provider);

            Dictionary<string, string> _documentsDict = new Dictionary<string, string>();

            try
            {
                string CandidateId = provider.FormData.GetValues("CandidateId")[0];

                if (!IsCandidateIdPresent(Convert.ToInt32(CandidateId)))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, JObject.Parse("{success:false , Message: \"Candidate ID is not present\"}"));
                }

                if (provider.FileData.Count == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, JObject.Parse("{success:false , Message: \"Please upload at least one document\"}"));
                }               

                for (int i = 0; i < provider.FileData.Count; i++)
                {
                    String name = provider.FileData[i].Headers.ContentDisposition.Name.Replace("\"", "").Trim();
                    String fileName = provider.FileData[i].Headers.ContentDisposition.FileName.Replace("\"", "").Trim();
                    string localFileName = provider.FileData[i].LocalFileName.Replace("\"", "").Trim();
                    string destinationFileName = root + "\\" + fileName;

                    File.Copy(localFileName, destinationFileName);

                    _documentsDict.Add(name, destinationFileName);

                    if (File.Exists(localFileName))
                    {
                        File.Delete(localFileName);
                    }
                }
                SaveCandidateDocuments("PUT", Convert.ToInt32(CandidateId), _documentsDict);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Request.CreateResponse(HttpStatusCode.OK, JObject.Parse("{success:true, Message:\"Documents uploaded successfully\"}"));
        }

        [HttpPost]      
        public async Task<HttpResponseMessage> UploadDocuments()
        {           
            string root = HttpContext.Current.Server.MapPath("~/Images");
            var provider = new MultipartFormDataStreamProvider(root);
             
            var result = await Request.Content.ReadAsMultipartAsync(provider);            

            Dictionary<string, string> _documentsDict = new Dictionary<string, string>();

            try
            {
                string CandidateId = provider.FormData.GetValues("CandidateId")[0];

                if(!IsCandidateIdPresent(Convert.ToInt32(CandidateId)))
                {                   
                    return Request.CreateResponse(HttpStatusCode.BadRequest,JObject.Parse("{success:false , Message: \"Candidate ID is not present\"}"));                    
                }

                if (provider.FileData.Count == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, JObject.Parse("{success:false , Message: \"Please upload at least one document\"}"));
                }

                if (IsDocumentUploaded(Convert.ToInt32(CandidateId)))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, JObject.Parse("{success:false , Message: \"Documents are already uploaded\"}"));
                }               

                for (int i = 0; i < provider.FileData.Count; i++)
                {
                    String name = provider.FileData[i].Headers.ContentDisposition.Name.Replace("\"", "").Trim();
                    String fileName = provider.FileData[i].Headers.ContentDisposition.FileName.Replace("\"", "").Trim();
                    string localFileName = provider.FileData[i].LocalFileName.Replace("\"", "").Trim();
                    string destinationFileName = root + "\\" + fileName;

                    File.Copy(localFileName, destinationFileName);

                    _documentsDict.Add(name, destinationFileName);

                    if (File.Exists(localFileName))
                    {
                        File.Delete(localFileName);
                    }
                }
                SaveCandidateDocuments("POST",Convert.ToInt32(CandidateId), _documentsDict);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return Request.CreateResponse(HttpStatusCode.Created, JObject.Parse("{success:true, Message:\"Documents uploaded successfully\"}"));
        }


        public static bool IsCandidateIdPresent(int candidateId)
        {
            int res = new DataAccessLayer().FindNumberOfRecords(candidateId, "candidate_registration");
            return res != 0?true:false;
        }

        public static bool IsDocumentUploaded(int candidateId)
        {
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();
            string fileReferenceId = _dataAccessLayer.GetFileReferenceId(candidateId);
            int res = _dataAccessLayer.FindNumberOfRecords(Convert.ToInt32(fileReferenceId), "file_storage");
            return res > 0 ? true : false;
        }


        public static void SaveCandidateDocuments(string method, int candidateId, Dictionary<string, string> _documents)
        {
            FileStorageDTO _fileStorage = new FileStorageDTO();
            DataAccessLayer _dataAccessLayer = new DataAccessLayer();
            try
            {
                if(_documents.ContainsKey("PanCard"))
                {
                    _fileStorage.Pancard_EXT = Path.GetExtension(_documents["PanCard"]);
                    _fileStorage.Pancard = ConvertToFileIntoBlobFormat(_documents["PanCard"]);
                    File.Delete(_documents["PanCard"]);
                }
                else
                {
                    _fileStorage.Pancard_EXT = null;
                    _fileStorage.Pancard = null;
                }

                if (_documents.ContainsKey("AdharCard"))
                {
                    _fileStorage.Adharcard_EXT = Path.GetExtension(_documents["AdharCard"]);
                    _fileStorage.Adharcard = ConvertToFileIntoBlobFormat(_documents["AdharCard"]);
                    File.Delete(_documents["AdharCard"]);
                }
                else
                {
                    _fileStorage.Adharcard_EXT = null;
                    _fileStorage.Adharcard = null;
                }

                if (_documents.ContainsKey("RationCard"))
                {
                    _fileStorage.Rationcard_EXT = Path.GetExtension(_documents["RationCard"]);
                    _fileStorage.Rationcard = ConvertToFileIntoBlobFormat(_documents["RationCard"]);
                    File.Delete(_documents["RationCard"]);
                }
                else
                {
                    _fileStorage.Rationcard_EXT = null;
                    _fileStorage.Rationcard = null;
                }

                if (_documents.ContainsKey("VoterCard"))
                {
                    _fileStorage.Votercard_EXT = Path.GetExtension(_documents["VoterCard"]);
                    _fileStorage.Votercard = ConvertToFileIntoBlobFormat(_documents["VoterCard"]);
                    File.Delete(_documents["VoterCard"]);
                }
                else
                {
                    _fileStorage.Votercard_EXT = null;
                    _fileStorage.Votercard = null;
                }

                if (_documents.ContainsKey("Passport"))
                {
                    _fileStorage.Passport_EXT = Path.GetExtension(_documents["Passport"]);
                    _fileStorage.Passport = ConvertToFileIntoBlobFormat(_documents["Passport"]);
                    File.Delete(_documents["Passport"]);
                }
                else
                {
                    _fileStorage.Passport_EXT = null;
                    _fileStorage.Passport = null;
                }

                if (_documents.ContainsKey("DrivingLicence"))
                {
                    _fileStorage.DrivingLicence_EXT = Path.GetExtension(_documents["DrivingLicence"]);
                    _fileStorage.DrivingLicence = ConvertToFileIntoBlobFormat(_documents["DrivingLicence"]);
                    File.Delete(_documents["DrivingLicence"]);
                }
                else
                {
                    _fileStorage.DrivingLicence_EXT = null;
                    _fileStorage.DrivingLicence = null;
                }

                if (_documents.ContainsKey("AcademicCertificate1"))
                {
                    _fileStorage.AcademicCertificate1_EXT = Path.GetExtension(_documents["AcademicCertificate1"]);
                    _fileStorage.AcademicCertificate1 = ConvertToFileIntoBlobFormat(_documents["AcademicCertificate1"]);
                    File.Delete(_documents["AcademicCertificate1"]);
                }
                else
                {
                    _fileStorage.AcademicCertificate1_EXT = null;
                    _fileStorage.AcademicCertificate1 = null;
                }

                if (_documents.ContainsKey("AcademicCertificate2"))
                {
                    _fileStorage.AcademicCertificate2_EXT = Path.GetExtension(_documents["AcademicCertificate2"]);
                    _fileStorage.AcademicCertificate2 = ConvertToFileIntoBlobFormat(_documents["AcademicCertificate2"]);
                    File.Delete(_documents["AcademicCertificate2"]);
                }
                else
                {
                    _fileStorage.AcademicCertificate2_EXT = null;
                    _fileStorage.AcademicCertificate2 = null;
                }

                if (_documents.ContainsKey("AcademicCertificate3"))
                {
                    _fileStorage.AcademicCertificate3_EXT = Path.GetExtension(_documents["AcademicCertificate3"]);
                    _fileStorage.AcademicCertificate3 = ConvertToFileIntoBlobFormat(_documents["AcademicCertificate3"]);
                    File.Delete(_documents["AcademicCertificate3"]);
                }
                else
                {
                    _fileStorage.AcademicCertificate3_EXT = null;
                    _fileStorage.AcademicCertificate3 = null;
                }

                if (_documents.ContainsKey("AcademicCertificate4"))
                {
                    _fileStorage.AcademicCertificate4_EXT = Path.GetExtension(_documents["AcademicCertificate4"]);
                    _fileStorage.AcademicCertificate4 = ConvertToFileIntoBlobFormat(_documents["AcademicCertificate4"]);
                    File.Delete(_documents["AcademicCertificate4"]);
                }
                else
                {
                    _fileStorage.AcademicCertificate4_EXT = null;
                    _fileStorage.AcademicCertificate4 = null;
                }

                if (_documents.ContainsKey("RelievingLetter1"))
                {
                    _fileStorage.RelievingLetter1_EXT = Path.GetExtension(_documents["RelievingLetter1"]);
                    _fileStorage.RelievingLetter1 = ConvertToFileIntoBlobFormat(_documents["RelievingLetter1"]);
                    File.Delete(_documents["RelievingLetter1"]);
                }
                else
                {
                    _fileStorage.RelievingLetter1_EXT = null;
                    _fileStorage.RelievingLetter1 = null;
                }

                if (_documents.ContainsKey("SalarySlip1"))
                {
                    _fileStorage.SalarySlip1_EXT = Path.GetExtension(_documents["SalarySlip1"]);
                    _fileStorage.SalarySlip1 = ConvertToFileIntoBlobFormat(_documents["SalarySlip1"]);
                    File.Delete(_documents["SalarySlip1"]);
                }
                else
                {
                    _fileStorage.SalarySlip1_EXT = null;
                    _fileStorage.SalarySlip1 = null;
                }

                if (_documents.ContainsKey("RelievingLetter2"))
                {
                    _fileStorage.RelievingLetter2_EXT = Path.GetExtension(_documents["RelievingLetter2"]);
                    _fileStorage.RelievingLetter2 = ConvertToFileIntoBlobFormat(_documents["RelievingLetter2"]);
                    File.Delete(_documents["RelievingLetter2"]);
                }
                else
                {
                    _fileStorage.RelievingLetter2_EXT = null;
                    _fileStorage.RelievingLetter2 = null;
                }

                if (_documents.ContainsKey("SalarySlip2"))
                {
                    _fileStorage.SalarySlip2_EXT = Path.GetExtension(_documents["SalarySlip2"]);
                    _fileStorage.SalarySlip2 = ConvertToFileIntoBlobFormat(_documents["SalarySlip2"]);
                    File.Delete(_documents["SalarySlip2"]);
                }
                else
                {
                    _fileStorage.SalarySlip2_EXT = null;
                    _fileStorage.SalarySlip2 = null;
                }

                if (_documents.ContainsKey("RelievingLetter3"))
                {
                    _fileStorage.RelievingLetter3_EXT = Path.GetExtension(_documents["RelievingLetter3"]);
                    _fileStorage.RelievingLetter3 = ConvertToFileIntoBlobFormat(_documents["RelievingLetter3"]);
                    File.Delete(_documents["RelievingLetter3"]);
                }
                else
                {
                    _fileStorage.RelievingLetter3_EXT = null;
                    _fileStorage.RelievingLetter3 = null;
                }

                if (_documents.ContainsKey("SalarySlip3"))
                {
                    _fileStorage.SalarySlip3_EXT = Path.GetExtension(_documents["SalarySlip3"]);
                    _fileStorage.SalarySlip3 = ConvertToFileIntoBlobFormat(_documents["SalarySlip3"]);
                    File.Delete(_documents["SalarySlip3"]);
                }
                else
                {
                    _fileStorage.SalarySlip3_EXT = null;
                    _fileStorage.SalarySlip3 = null;
                }

                if (_documents.ContainsKey("Signature"))
                {
                    _fileStorage.Signature_EXT = Path.GetExtension(_documents["Signature"]);
                    _fileStorage.Signature = ConvertToFileIntoBlobFormat(_documents["Signature"]);
                    File.Delete(_documents["Signature"]);
                }
                else
                {
                    _fileStorage.Signature_EXT = null;
                    _fileStorage.Signature = null;
                }
                if(String.Equals(method.ToUpper(),"PUT"))
                {
                    _fileStorage = _dataAccessLayer.GetFilesForExistingCandidate(candidateId, _fileStorage);
                }
                int res = _dataAccessLayer.SubmitCandidateDocuments(method, candidateId, _fileStorage);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        public static byte[] ConvertToFileIntoBlobFormat(string fileName)
        {
            using (Stream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    return bytes;
                }
            }
                
        }
        
        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
