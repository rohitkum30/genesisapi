﻿using GenesisWebAPI.DALLayer;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace GenesisWebAPI.APIMain.Controllers
{
    public class OrganizationController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        
        [HttpGet]       
        public IHttpActionResult Get(string organizationName)
        {
            HttpContext httpContext = HttpContext.Current;
            var authHeader = httpContext.Request.Headers["Authorization"];
            string organizationId = new DataAccessLayer().GetOrganizationId(organizationName);
            if(String.IsNullOrEmpty(organizationId))
            {
                return Content(HttpStatusCode.BadRequest, JObject.Parse("{success:false, Message:\"Please enter a valid organization name.\"}"));
            }
            return Content(HttpStatusCode.OK, JObject.Parse("{success:true,organizationid:\"" + organizationId + "\"}"));
        }

        
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
