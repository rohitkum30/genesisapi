﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccess;
using System.Data;
using GenesisWebApi.DTO.ResponseDTOs;
using GenesisWebApi.DTO.RequestDTOs;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Web;
using System.IO;

namespace GenesisWebAPI.DALLayer
{
    public class DataAccessLayer : IDisposable
    {

        MySqlConnection conn = null;
        string ConnectionString = string.Empty;

        public IEnumerable<DataRow> GetAllCandidate()
        {
            DataTable dtCandidateData = new DataTable();
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                string sqlString = "select * from candidate_registration Limit 10; ";
                dtCandidateData = DataAccess.GetDataTable(conn, sqlString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return dtCandidateData.AsEnumerable().ToList();
        }

        public CandidateRequestDTO[] GetAllCandidatesOfOrganization(string organizationId)
        {
            DataTable dtAllCandidatesData = new DataTable();
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null; CandidateRequestDTO[] allCandidateData = null;
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                string sqlString = "select * from candidate_registration where organization_id = "+organizationId+";";
                dtAllCandidatesData = DataAccess.GetDataTable(conn, sqlString);

                allCandidateData = new CandidateRequestDTO[dtAllCandidatesData.Rows.Count];

                for(int i=0; i< dtAllCandidatesData.Rows.Count; i++)
                {
                    CandidateRequestDTO _candidateRequsetDTO = new CandidateRequestDTO();
                    int candidateId = Convert.ToInt32(dtAllCandidatesData.Rows[i]["CANDIDATE_ID"].ToString());
                    _candidateRequsetDTO.CandidateId = dtAllCandidatesData.Rows[i]["CANDIDATE_ID"].ToString();
                    _candidateRequsetDTO.OrganizationId = dtAllCandidatesData.Rows[i]["ORGANIZATION_ID"].ToString();
                    _candidateRequsetDTO.Name = dtAllCandidatesData.Rows[i]["Name"].ToString();
                    _candidateRequsetDTO.FathersName = dtAllCandidatesData.Rows[i]["FATHERS_NAME"].ToString();
                    _candidateRequsetDTO.SurName = dtAllCandidatesData.Rows[0]["SURNAME"].ToString();
                    _candidateRequsetDTO.SchoolOrCollegeName1 = dtAllCandidatesData.Rows[i]["SCHOOLORCOLLEGE_NAME1"].ToString();
                    _candidateRequsetDTO.DateOfEntering1 = dtAllCandidatesData.Rows[i]["DATE_OF_ENTERING1"].ToString(); ;
                    _candidateRequsetDTO.DateOfLeaving1 = dtAllCandidatesData.Rows[i]["DATE_OF_LEAVING1"].ToString();
                    _candidateRequsetDTO.ExamPassed1 = dtAllCandidatesData.Rows[i]["EXAMINATION_PASSED1"].ToString();
                    _candidateRequsetDTO.SchoolOrCollegeName2 = dtAllCandidatesData.Rows[i]["SCHOOLORCOLLEGE_NAME2"].ToString();
                    _candidateRequsetDTO.DateOfEntering2 = dtAllCandidatesData.Rows[i]["DATE_OF_ENTERING2"].ToString(); ;
                    _candidateRequsetDTO.DateOfLeaving2 = dtAllCandidatesData.Rows[i]["DATE_OF_LEAVING2"].ToString();
                    _candidateRequsetDTO.ExamPassed2 = dtAllCandidatesData.Rows[i]["EXAMINATION_PASSED2"].ToString();
                    _candidateRequsetDTO.SchoolOrCollegeName3 = dtAllCandidatesData.Rows[i]["SCHOOLORCOLLEGE_NAME3"].ToString();
                    _candidateRequsetDTO.DateOfEntering3 = dtAllCandidatesData.Rows[i]["DATE_OF_ENTERING3"].ToString(); ;
                    _candidateRequsetDTO.DateOfLeaving3 = dtAllCandidatesData.Rows[i]["DATE_OF_LEAVING3"].ToString();
                    _candidateRequsetDTO.ExamPassed3 = dtAllCandidatesData.Rows[i]["EXAMINATION_PASSED3"].ToString();
                    bool isPendingCriminalCases = dtAllCandidatesData.Rows[i]["PENDING_CRIMINAL_CASE"].ToString().Equals("1") ? true : false;
                    _candidateRequsetDTO.Pending_Criminal_Case = isPendingCriminalCases;
                    bool isPendingEducationCases = dtAllCandidatesData.Rows[i]["PENDING_EDUCATIONAL_CASE"].ToString().Equals("1") ? true : false;
                    _candidateRequsetDTO.Pending_Educational_Case = isPendingEducationCases;
                    _candidateRequsetDTO.Pending_Case_Detail = dtAllCandidatesData.Rows[i]["PENDING_CASE_DETAILS"].ToString();
                    _candidateRequsetDTO.PassportNumber = dtAllCandidatesData.Rows[i]["PASSPORT_NUMBER"].ToString();
                    _candidateRequsetDTO.PassportIssueDate = dtAllCandidatesData.Rows[i]["PASSPORT_ISSUE_DATE"].ToString();
                    _candidateRequsetDTO.PassportPlaceOfIssue = dtAllCandidatesData.Rows[i]["PASSPORT_PLACE_OF_ISSUE"].ToString();
                    _candidateRequsetDTO.PassportIssuingAuthority = dtAllCandidatesData.Rows[i]["PASSPORT_ISSUING_AUTHORITY"].ToString();
                    _candidateRequsetDTO.ValidUpto = dtAllCandidatesData.Rows[i]["VALID_UPTO"].ToString();
                    _candidateRequsetDTO.SubmissionPlace = dtAllCandidatesData.Rows[i]["CANDIDATE_INFO_SUB_PLACE"].ToString();

                    String addressQuery = "select * from address where candidate_id=" + candidateId + ";";
                    DataTable dtAddressData = DataAccess.GetDataTable(conn, addressQuery);

                    var row = dtAddressData.AsEnumerable().Select((r, z) => new { Row = r, Index = z }).Where(x => x.Row["ADDRESS_TYPE"].ToString() == "Present").FirstOrDefault();
                    int presentRowIndex = row.Index;
                    row = dtAddressData.AsEnumerable().Select((r, z) => new { Row = r, Index = z }).Where(x => x.Row["ADDRESS_TYPE"].ToString() == "Permanent").FirstOrDefault();
                    int permanentRowIndex = row.Index;
                    AddressRequestDTO _presentAddressRequestDTO = new AddressRequestDTO();
                    _presentAddressRequestDTO.Address = dtAddressData.Rows[presentRowIndex]["ADDRESS"].ToString();
                    _presentAddressRequestDTO.LandlineNumber = dtAddressData.Rows[presentRowIndex]["LANDLINE_NUMBER"].ToString();
                    _presentAddressRequestDTO.MobileNumber = dtAddressData.Rows[presentRowIndex]["MOBILE_NUMBER"].ToString();
                    _presentAddressRequestDTO.City = dtAddressData.Rows[presentRowIndex]["CITY"].ToString();
                    _presentAddressRequestDTO.State = dtAddressData.Rows[presentRowIndex]["STATE"].ToString();
                    _presentAddressRequestDTO.Landmark = dtAddressData.Rows[presentRowIndex]["LANDMARK"].ToString();
                    _presentAddressRequestDTO.PostalCode = dtAddressData.Rows[presentRowIndex]["POSTAL_CODE"].ToString();
                    _presentAddressRequestDTO.PoliceStationName = dtAddressData.Rows[presentRowIndex]["POLICE_STATION_NAME"].ToString();

                    AddressRequestDTO _permanentAddressRequestDTO = new AddressRequestDTO();
                    _permanentAddressRequestDTO.Address = dtAddressData.Rows[permanentRowIndex]["ADDRESS"].ToString();
                    _permanentAddressRequestDTO.LandlineNumber = dtAddressData.Rows[permanentRowIndex]["LANDLINE_NUMBER"].ToString();
                    _permanentAddressRequestDTO.MobileNumber = dtAddressData.Rows[permanentRowIndex]["MOBILE_NUMBER"].ToString();
                    _permanentAddressRequestDTO.City = dtAddressData.Rows[permanentRowIndex]["CITY"].ToString();
                    _permanentAddressRequestDTO.State = dtAddressData.Rows[permanentRowIndex]["STATE"].ToString();
                    _permanentAddressRequestDTO.Landmark = dtAddressData.Rows[permanentRowIndex]["LANDMARK"].ToString();
                    _permanentAddressRequestDTO.PostalCode = dtAddressData.Rows[permanentRowIndex]["POSTAL_CODE"].ToString();
                    _permanentAddressRequestDTO.PoliceStationName = dtAddressData.Rows[permanentRowIndex]["POLICE_STATION_NAME"].ToString();

                    _candidateRequsetDTO.PresentAddress = _presentAddressRequestDTO;
                    _candidateRequsetDTO.PermanentAddress = _permanentAddressRequestDTO;

                    string employerHistoryQuery = "select * from candidate_emp_history where candidate_id =" + candidateId + ";";
                    DataTable dtEmployerHistData = DataAccess.GetDataTable(conn, employerHistoryQuery);

                    int noOfEmployerHistoryRows = dtEmployerHistData.Rows.Count;
                    _candidateRequsetDTO.employeeHistory = new EmployerHistoryRequestDTO[noOfEmployerHistoryRows];

                    for (int j = 0; j < noOfEmployerHistoryRows; j++)
                    {
                        EmployerHistoryRequestDTO _empHistory = new EmployerHistoryRequestDTO();
                        _empHistory.EmployeeId = dtEmployerHistData.Rows[j]["EMPLOYEE_ID"].ToString();
                        _empHistory.EmployerName = dtEmployerHistData.Rows[j]["EMPLOYER_NAME"].ToString();
                        _empHistory.FromDate = dtEmployerHistData.Rows[j]["FROM_DATE"].ToString();
                        _empHistory.ToDate = dtEmployerHistData.Rows[j]["TO_DATE"].ToString();
                        _empHistory.StreetAddress = dtEmployerHistData.Rows[j]["STREET_ADDRESS"].ToString();
                        _empHistory.EmployerLandlineNo = dtEmployerHistData.Rows[j]["EMPLOYERS_LANDLINE_NO"].ToString();
                        _empHistory.City = dtEmployerHistData.Rows[j]["CITY"].ToString();
                        _empHistory.State = dtEmployerHistData.Rows[j]["STATE"].ToString();
                        _empHistory.Country = dtEmployerHistData.Rows[j]["COUNTRY"].ToString();
                        _empHistory.PostalCode = dtEmployerHistData.Rows[j]["POSTAL_CODE"].ToString();
                        _empHistory.JobTitle = dtEmployerHistData.Rows[j]["JOB_TITLE"].ToString();
                        _empHistory.ReasonForLeaving = dtEmployerHistData.Rows[j]["REASON_FOR_LEAVING"].ToString();
                        string employmentType = dtEmployerHistData.Rows[j]["EMPLOYMENT_TYPE"].ToString();
                        employmentType = employmentType.Equals("1") ? "Full Time" : "Contractor";
                        _empHistory.EmploymentType = employmentType;
                        _empHistory.CTC = dtEmployerHistData.Rows[j]["CTC"].ToString();
                        _empHistory.OAD_Name = dtEmployerHistData.Rows[j]["OAD_NAME"].ToString();
                        _empHistory.OAD_Address = dtEmployerHistData.Rows[j]["OAD_ADDRESS"].ToString();
                        _empHistory.OAD_LandlineNo = dtEmployerHistData.Rows[j]["OAD_LANDLINE_NO"].ToString();
                        _empHistory.SupervisorName = dtEmployerHistData.Rows[j]["SUPERVISOR_NAME"].ToString();
                        _empHistory.SupervisorTitle = dtEmployerHistData.Rows[j]["SUPERVISOR_TITLE"].ToString();
                        _empHistory.SupervisorLandlineNo = dtEmployerHistData.Rows[j]["SUPERVISOR_LANDLINE_NO"].ToString();
                        _empHistory.SupervisorEmailAddress = dtEmployerHistData.Rows[j]["SUPERVISOR_EMAIL_ADDRESS"].ToString();
                        _empHistory.HR_Name = dtEmployerHistData.Rows[j]["HR_NAME"].ToString();
                        _empHistory.HR_Title = dtEmployerHistData.Rows[j]["HR_TITLE"].ToString();
                        _empHistory.HR_LandlineNo = dtEmployerHistData.Rows[j]["HR_LANDLINE_NO"].ToString();
                        _empHistory.HR_EmailAddress = dtEmployerHistData.Rows[j]["HR_EMAIL_ADDRESS"].ToString();
                        _empHistory.Rep_Manager_Name = dtEmployerHistData.Rows[i]["REP_MANAGER_NAME"].ToString();
                        _empHistory.Rep_Manager_Company_Name = dtEmployerHistData.Rows[i]["REP_MANAGER_COMPANY_NAME"].ToString();
                        _empHistory.Rep_Manager_Title = dtEmployerHistData.Rows[i]["REP_MANAGER_TITLE"].ToString();
                        _empHistory.Rep_Manager_Mobile_No = dtEmployerHistData.Rows[i]["REP_MANAGER_MOBILE_NO"].ToString();
                        _empHistory.Rep_Manager_EmailID = dtEmployerHistData.Rows[i]["REP_MANAGER_EMAILID"].ToString();

                        _candidateRequsetDTO.employeeHistory[j] = _empHistory;
                    }
                    allCandidateData[i] = _candidateRequsetDTO;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return allCandidateData;
        }

        public bool CheckValidUserFromOrganization(int organizationId, string user, string pwd)
        {
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null; bool isValidUser = true;
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                string sqlString = "select * from login where username='"+user+"' and password='"+pwd+"' and organization_id="+organizationId+";";
                DataTable dtLogin = DataAccess.GetDataTable(conn, sqlString);
                if(dtLogin.Rows.Count == 0)
                {
                    isValidUser = false;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return isValidUser;
        }

        public string GetOrganizationId(string organizationName)
        {
            DataTable dtOrganizationData = new DataTable();
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null; string organizationId = null;
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                string sqlString = "select Organization_ID from organizations where organization_name='" + organizationName + "';";
                dtOrganizationData = DataAccess.GetDataTable(conn, sqlString);
                if (dtOrganizationData.Rows.Count > 0)
                {
                    organizationId = dtOrganizationData.Rows[0]["Organization_ID"].ToString();                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return organizationId;
        }

        public CandidateRequestDTO GetCandidateData(long candidateId)
        {
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null;         
            CandidateRequestDTO _candidateRequsetDTO = new CandidateRequestDTO();
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                string sqlString = "select * from candidate_registration where candidate_id=" + candidateId + ";";
                DataTable dtCandidateData = DataAccess.GetDataTable(conn, sqlString);

                if (dtCandidateData.Rows.Count > 0)
                {
                    _candidateRequsetDTO.CandidateId = dtCandidateData.Rows[0]["CANDIDATE_ID"].ToString();
                    _candidateRequsetDTO.OrganizationId = dtCandidateData.Rows[0]["ORGANIZATION_ID"].ToString();
                    _candidateRequsetDTO.Name = dtCandidateData.Rows[0]["Name"].ToString();
                    _candidateRequsetDTO.FathersName = dtCandidateData.Rows[0]["FATHERS_NAME"].ToString();
                    _candidateRequsetDTO.SurName = dtCandidateData.Rows[0]["SURNAME"].ToString();
                    _candidateRequsetDTO.SchoolOrCollegeName1 = dtCandidateData.Rows[0]["SCHOOLORCOLLEGE_NAME1"].ToString();
                    _candidateRequsetDTO.DateOfEntering1 = dtCandidateData.Rows[0]["DATE_OF_ENTERING1"].ToString(); ;
                    _candidateRequsetDTO.DateOfLeaving1 = dtCandidateData.Rows[0]["DATE_OF_LEAVING1"].ToString();
                    _candidateRequsetDTO.ExamPassed1 = dtCandidateData.Rows[0]["EXAMINATION_PASSED1"].ToString();
                    _candidateRequsetDTO.SchoolOrCollegeName2 = dtCandidateData.Rows[0]["SCHOOLORCOLLEGE_NAME2"].ToString();
                    _candidateRequsetDTO.DateOfEntering2 = dtCandidateData.Rows[0]["DATE_OF_ENTERING2"].ToString(); ;
                    _candidateRequsetDTO.DateOfLeaving2 = dtCandidateData.Rows[0]["DATE_OF_LEAVING2"].ToString();
                    _candidateRequsetDTO.ExamPassed2 = dtCandidateData.Rows[0]["EXAMINATION_PASSED2"].ToString();
                    _candidateRequsetDTO.SchoolOrCollegeName3 = dtCandidateData.Rows[0]["SCHOOLORCOLLEGE_NAME3"].ToString();
                    _candidateRequsetDTO.DateOfEntering3 = dtCandidateData.Rows[0]["DATE_OF_ENTERING3"].ToString(); ;
                    _candidateRequsetDTO.DateOfLeaving3 = dtCandidateData.Rows[0]["DATE_OF_LEAVING3"].ToString();
                    _candidateRequsetDTO.ExamPassed3 = dtCandidateData.Rows[0]["EXAMINATION_PASSED3"].ToString();
                    _candidateRequsetDTO.SchoolOrCollegeName4 = dtCandidateData.Rows[0]["SCHOOLORCOLLEGE_NAME4"].ToString();
                    _candidateRequsetDTO.DateOfEntering4 = dtCandidateData.Rows[0]["DATE_OF_ENTERING4"].ToString(); ;
                    _candidateRequsetDTO.DateOfLeaving4 = dtCandidateData.Rows[0]["DATE_OF_LEAVING4"].ToString();
                    _candidateRequsetDTO.ExamPassed4 = dtCandidateData.Rows[0]["EXAMINATION_PASSED4"].ToString();
                    bool isPendingCriminalCases = dtCandidateData.Rows[0]["PENDING_CRIMINAL_CASE"].ToString().Equals("1") ? true:false;
                    _candidateRequsetDTO.Pending_Criminal_Case = isPendingCriminalCases;
                    bool isPendingEducationCases = dtCandidateData.Rows[0]["PENDING_EDUCATIONAL_CASE"].ToString().Equals("1") ? true : false;
                    _candidateRequsetDTO.Pending_Educational_Case = isPendingEducationCases;
                    _candidateRequsetDTO.Pending_Case_Detail = dtCandidateData.Rows[0]["PENDING_CASE_DETAILS"].ToString();
                    _candidateRequsetDTO.PassportNumber = dtCandidateData.Rows[0]["PASSPORT_NUMBER"].ToString();
                    _candidateRequsetDTO.PassportIssueDate = dtCandidateData.Rows[0]["PASSPORT_ISSUE_DATE"].ToString();
                    _candidateRequsetDTO.PassportPlaceOfIssue = dtCandidateData.Rows[0]["PASSPORT_PLACE_OF_ISSUE"].ToString();
                    _candidateRequsetDTO.PassportIssuingAuthority = dtCandidateData.Rows[0]["PASSPORT_ISSUING_AUTHORITY"].ToString();
                    _candidateRequsetDTO.ValidUpto = dtCandidateData.Rows[0]["VALID_UPTO"].ToString();
                    _candidateRequsetDTO.SubmissionPlace = dtCandidateData.Rows[0]["CANDIDATE_INFO_SUB_PLACE"].ToString();

                    
                    String addressQuery = "select * from address where candidate_id="+candidateId+";";
                    DataTable dtAddressData = DataAccess.GetDataTable(conn, addressQuery);

                    var row = dtAddressData.AsEnumerable().Select((r, i) => new { Row = r, Index = i }).Where(x => x.Row["ADDRESS_TYPE"].ToString() == "Present").FirstOrDefault();
                    int presentRowIndex = row.Index;
                    row = dtAddressData.AsEnumerable().Select((r, i) => new { Row = r, Index = i }).Where(x => x.Row["ADDRESS_TYPE"].ToString() == "Permanent").FirstOrDefault();
                    int permanentRowIndex = row.Index;
                    AddressRequestDTO _presentAddressRequestDTO = new AddressRequestDTO();
                    _presentAddressRequestDTO.Address = dtAddressData.Rows[presentRowIndex]["ADDRESS"].ToString();                    
                    _presentAddressRequestDTO.LandlineNumber = dtAddressData.Rows[presentRowIndex]["LANDLINE_NUMBER"].ToString();
                    _presentAddressRequestDTO.MobileNumber = dtAddressData.Rows[presentRowIndex]["MOBILE_NUMBER"].ToString();
                    _presentAddressRequestDTO.Alt_Mobile_Number = dtAddressData.Rows[presentRowIndex]["ALT_MOBILE_NUMBER"].ToString();
                    _presentAddressRequestDTO.City = dtAddressData.Rows[presentRowIndex]["CITY"].ToString();
                    _presentAddressRequestDTO.State = dtAddressData.Rows[presentRowIndex]["STATE"].ToString();
                    _presentAddressRequestDTO.Landmark = dtAddressData.Rows[presentRowIndex]["LANDMARK"].ToString();
                    _presentAddressRequestDTO.PostalCode = dtAddressData.Rows[presentRowIndex]["POSTAL_CODE"].ToString();
                    _presentAddressRequestDTO.PoliceStationName = dtAddressData.Rows[presentRowIndex]["POLICE_STATION_NAME"].ToString();

                    AddressRequestDTO _permanentAddressRequestDTO = new AddressRequestDTO();
                    _permanentAddressRequestDTO.Address = dtAddressData.Rows[permanentRowIndex]["ADDRESS"].ToString();
                    _permanentAddressRequestDTO.LandlineNumber = dtAddressData.Rows[permanentRowIndex]["LANDLINE_NUMBER"].ToString();
                    _permanentAddressRequestDTO.MobileNumber = dtAddressData.Rows[permanentRowIndex]["MOBILE_NUMBER"].ToString();
                    _permanentAddressRequestDTO.Alt_Mobile_Number = dtAddressData.Rows[permanentRowIndex]["ALT_MOBILE_NUMBER"].ToString();
                    _permanentAddressRequestDTO.City = dtAddressData.Rows[permanentRowIndex]["CITY"].ToString();
                    _permanentAddressRequestDTO.State = dtAddressData.Rows[permanentRowIndex]["STATE"].ToString();
                    _permanentAddressRequestDTO.Landmark = dtAddressData.Rows[permanentRowIndex]["LANDMARK"].ToString();
                    _permanentAddressRequestDTO.PostalCode = dtAddressData.Rows[permanentRowIndex]["POSTAL_CODE"].ToString();
                    _permanentAddressRequestDTO.PoliceStationName = dtAddressData.Rows[permanentRowIndex]["POLICE_STATION_NAME"].ToString();

                    _candidateRequsetDTO.PresentAddress = _presentAddressRequestDTO;
                    _candidateRequsetDTO.PermanentAddress = _permanentAddressRequestDTO;

                    string employerHistoryQuery = "select * from candidate_emp_history where candidate_id ="+candidateId+";";
                    DataTable dtEmployerHistData = DataAccess.GetDataTable(conn, employerHistoryQuery);

                    int noOfEmployerHistoryRows = dtEmployerHistData.Rows.Count;
                    _candidateRequsetDTO.employeeHistory = new EmployerHistoryRequestDTO[noOfEmployerHistoryRows];

                    for(int i=0; i< noOfEmployerHistoryRows; i++)
                    {
                        EmployerHistoryRequestDTO _empHistory = new EmployerHistoryRequestDTO();
                        _empHistory.EmployeeId = dtEmployerHistData.Rows[i]["EMPLOYEE_ID"].ToString();
                        _empHistory.EmployerName = dtEmployerHistData.Rows[i]["EMPLOYER_NAME"].ToString();
                        _empHistory.FromDate = dtEmployerHistData.Rows[i]["FROM_DATE"].ToString();
                        _empHistory.ToDate = dtEmployerHistData.Rows[i]["TO_DATE"].ToString();
                        _empHistory.StreetAddress = dtEmployerHistData.Rows[i]["STREET_ADDRESS"].ToString();
                        _empHistory.EmployerLandlineNo = dtEmployerHistData.Rows[i]["EMPLOYERS_LANDLINE_NO"].ToString();
                        _empHistory.City = dtEmployerHistData.Rows[i]["CITY"].ToString();
                        _empHistory.State = dtEmployerHistData.Rows[i]["STATE"].ToString();
                        _empHistory.Country = dtEmployerHistData.Rows[i]["COUNTRY"].ToString();
                        _empHistory.PostalCode = dtEmployerHistData.Rows[i]["POSTAL_CODE"].ToString();
                        _empHistory.JobTitle = dtEmployerHistData.Rows[i]["JOB_TITLE"].ToString();
                        _empHistory.ReasonForLeaving = dtEmployerHistData.Rows[i]["REASON_FOR_LEAVING"].ToString();
                        string employmentType = dtEmployerHistData.Rows[i]["EMPLOYMENT_TYPE"].ToString();
                        employmentType = employmentType.Equals("1") ? "Full Time" : "Contractor";
                        _empHistory.EmploymentType = employmentType;
                        _empHistory.CTC = dtEmployerHistData.Rows[i]["CTC"].ToString();
                        _empHistory.OAD_Name = dtEmployerHistData.Rows[i]["OAD_NAME"].ToString();
                        _empHistory.OAD_Address = dtEmployerHistData.Rows[i]["OAD_ADDRESS"].ToString();
                        _empHistory.OAD_LandlineNo = dtEmployerHistData.Rows[i]["OAD_LANDLINE_NO"].ToString();
                        _empHistory.SupervisorName = dtEmployerHistData.Rows[i]["SUPERVISOR_NAME"].ToString();
                        _empHistory.SupervisorTitle = dtEmployerHistData.Rows[i]["SUPERVISOR_TITLE"].ToString();
                        _empHistory.SupervisorLandlineNo = dtEmployerHistData.Rows[i]["SUPERVISOR_LANDLINE_NO"].ToString();
                        _empHistory.SupervisorEmailAddress = dtEmployerHistData.Rows[i]["SUPERVISOR_EMAIL_ADDRESS"].ToString();
                        _empHistory.HR_Name = dtEmployerHistData.Rows[i]["HR_NAME"].ToString();
                        _empHistory.HR_Title = dtEmployerHistData.Rows[i]["HR_TITLE"].ToString();
                        _empHistory.HR_LandlineNo = dtEmployerHistData.Rows[i]["HR_LANDLINE_NO"].ToString();
                        _empHistory.HR_EmailAddress = dtEmployerHistData.Rows[i]["HR_EMAIL_ADDRESS"].ToString();
                        _empHistory.Rep_Manager_Name = dtEmployerHistData.Rows[i]["REP_MANAGER_NAME"].ToString();
                        _empHistory.Rep_Manager_Company_Name = dtEmployerHistData.Rows[i]["REP_MANAGER_COMPANY_NAME"].ToString();
                        _empHistory.Rep_Manager_Title = dtEmployerHistData.Rows[i]["REP_MANAGER_TITLE"].ToString();
                        _empHistory.Rep_Manager_Mobile_No = dtEmployerHistData.Rows[i]["REP_MANAGER_MOBILE_NO"].ToString();
                        _empHistory.Rep_Manager_EmailID = dtEmployerHistData.Rows[i]["REP_MANAGER_EMAILID"].ToString();
                        _candidateRequsetDTO.employeeHistory[i] = _empHistory;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return _candidateRequsetDTO;
        }


        public string GetConnectionString()
        {
            string ConnectionString;

            if (System.Configuration.ConfigurationManager.ConnectionStrings["sample"] != null)
            {
                ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["sample"].ConnectionString;
            }
            else
            {
                throw new Exception("connection string not defined");
            }

            return ConnectionString;
        }

        private DataTable GetDataTable(MySqlConnection conn, CommandType cmdtype, string Sql)
        {
            MySqlCommand cmd;
            MySqlDataAdapter mySqlDataAdapter;

            cmd = new MySqlCommand(Sql, conn);
            cmd.CommandType = cmdtype;

            mySqlDataAdapter = new MySqlDataAdapter(Sql, conn);
            DataSet ds = new DataSet();
            mySqlDataAdapter.Fill(ds);
            return ds.Tables[0];
        }

        public DataTable GetDataTable(MySqlConnection conn, string Sql)
        {
            return GetDataTable(conn, CommandType.Text, Sql);
        }

        public MySqlConnection MySqlConnection
        {
            get
            {
                if (conn == null)
                {
                    ConnectionString = GetConnectionString();
                    conn = new MySqlConnection(ConnectionString);
                }
                return conn;
            }
        }

        public DataTable GetData(string sqlStmt)
        {
            try
            {
                if (MySqlConnection.State != ConnectionState.Open)
                    MySqlConnection.Open();
                return GetDataTable(MySqlConnection, sqlStmt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                MySqlConnection.Close();
            }
        } 
     

        public byte[] GetFinalReport(int _candidateID, out string _reportFileName)
        {
            string sqlString = "select * from candidate_verification_report where CANDIDATE_ID=" + _candidateID + ";";
            DataTable dtReportDownload = new DataTable();
            _reportFileName = string.Empty; byte[] reportBinaryData = null;
            using (DataAccessLayer dataAccessLayer = new DataAccessLayer())
            {
                dtReportDownload = dataAccessLayer.GetData(sqlString);
            }

            if (dtReportDownload.Rows.Count != 0)
            {
                string filePath = string.Empty;
                string reportFileExtension = string.Empty;
                if (!(dtReportDownload.Rows[0]["REPORT_FILE"] is System.DBNull))
                {
                    _reportFileName = "Final_Report";
                    reportFileExtension = (string)dtReportDownload.Rows[0]["REPORT_FILE_EXT"];
                    reportBinaryData = (byte[])dtReportDownload.Rows[0]["REPORT_FILE"];
                    _reportFileName = _candidateID + "_" + _reportFileName + "." + reportFileExtension;
                }
            }
            return reportBinaryData;
        }

        public int SubmitCandidateDetails(CandidateRequestDTO candidateData)
        {
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null;

            int noOfEmployerDetails = candidateData.employeeHistory != null ? candidateData.employeeHistory.Length : 0;

            Parameters parameters = new Parameters();
            parameters.Add(new MySqlParameter("Name_d", candidateData.Name));
            parameters.Add(new MySqlParameter("FathersName_d", candidateData.FathersName));
            parameters.Add(new MySqlParameter("Surname_d", candidateData.SurName));
            parameters.Add(new MySqlParameter("SchoolOrCollegeName1_d", candidateData.SchoolOrCollegeName1));
            if (string.IsNullOrEmpty(candidateData.DateOfEntering1))
            {
                parameters.Add(new MySqlParameter("DateOfEntering1_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfEntering1_d", Convert.ToDateTime(candidateData.DateOfEntering1)));
            }
            if (string.IsNullOrEmpty(candidateData.DateOfLeaving1))
            {
                parameters.Add(new MySqlParameter("DateOfLeaving1_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfLeaving1_d", Convert.ToDateTime(candidateData.DateOfLeaving1)));
            }
            parameters.Add(new MySqlParameter("ExaminationPassed1_d", candidateData.ExamPassed1));

            parameters.Add(new MySqlParameter("SchoolOrCollegeName2_d", candidateData.SchoolOrCollegeName2));
            if (string.IsNullOrEmpty(candidateData.DateOfEntering2))
            {
                parameters.Add(new MySqlParameter("DateOfEntering2_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfEntering2_d", Convert.ToDateTime(candidateData.DateOfEntering2)));
            }
            if (string.IsNullOrEmpty(candidateData.DateOfLeaving2))
            {
                parameters.Add(new MySqlParameter("DateOfLeaving2_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfLeaving2_d", Convert.ToDateTime(candidateData.DateOfLeaving2)));
            }
            parameters.Add(new MySqlParameter("ExaminationPassed2_d", candidateData.ExamPassed2));

            parameters.Add(new MySqlParameter("SchoolOrCollegeName3_d", candidateData.SchoolOrCollegeName3));
            if (string.IsNullOrEmpty(candidateData.DateOfEntering3))
            {
                parameters.Add(new MySqlParameter("DateOfEntering3_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfEntering3_d", Convert.ToDateTime(candidateData.DateOfEntering3)));
            }
            if (string.IsNullOrEmpty(candidateData.DateOfLeaving3))
            {
                parameters.Add(new MySqlParameter("DateOfLeaving3_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfLeaving3_d", Convert.ToDateTime(candidateData.DateOfLeaving3)));
            }
            parameters.Add(new MySqlParameter("ExaminationPassed3_d", candidateData.ExamPassed3));

            parameters.Add(new MySqlParameter("SchoolOrCollegeName4_d", candidateData.SchoolOrCollegeName4));
            if (string.IsNullOrEmpty(candidateData.DateOfEntering4))
            {
                parameters.Add(new MySqlParameter("DateOfEntering4_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfEntering4_d", Convert.ToDateTime(candidateData.DateOfEntering4)));
            }
            if (string.IsNullOrEmpty(candidateData.DateOfLeaving4))
            {
                parameters.Add(new MySqlParameter("DateOfLeaving4_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("DateOfLeaving4_d", Convert.ToDateTime(candidateData.DateOfLeaving4)));
            }
            parameters.Add(new MySqlParameter("ExaminationPassed4_d", candidateData.ExamPassed4));

            parameters.Add(new MySqlParameter("ReferenceName1_d", null));
            parameters.Add(new MySqlParameter("ReferenceMobileNo1_d", 0));
            parameters.Add(new MySqlParameter("ReferenceName2_d", null));
            parameters.Add(new MySqlParameter("ReferenceMobileNo2_d", 0));

            parameters.Add(new MySqlParameter("PendingCriminalCase_d", candidateData.Pending_Criminal_Case));
            parameters.Add(new MySqlParameter("PendingEducationalCase_d", candidateData.Pending_Educational_Case));
            parameters.Add(new MySqlParameter("PendingCaseDetail_d", candidateData.Pending_Case_Detail));

            parameters.Add(new MySqlParameter("PassportNumber_d", candidateData.PassportNumber));
            if (string.IsNullOrEmpty(candidateData.PassportIssueDate))
            {
                parameters.Add(new MySqlParameter("PassportIssueDate_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("PassportIssueDate_d", Convert.ToDateTime(candidateData.PassportIssueDate)));
            }
            parameters.Add(new MySqlParameter("PassportPlaceOfIssue_d", candidateData.PassportPlaceOfIssue));
            parameters.Add(new MySqlParameter("PassportIssuingAuthority_d", candidateData.PassportIssuingAuthority));
            if (string.IsNullOrEmpty(candidateData.ValidUpto))
            {
                parameters.Add(new MySqlParameter("ValidUpto_d", DateTime.MinValue));
            }
            else
            {
                parameters.Add(new MySqlParameter("ValidUpto_d", Convert.ToDateTime(candidateData.ValidUpto)));
            }

            parameters.Add(new MySqlParameter("CandidateInfoSbPlace_d", candidateData.SubmissionPlace));
            parameters.Add(new MySqlParameter("CandidateInfoSubDate_d", Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"))));
            parameters.Add(new MySqlParameter("CreatedBy_d", "API"));

            parameters.Add(new MySqlParameter("PresentAddressType_d", "Present"));
            parameters.Add(new MySqlParameter("PresentAddress_d", candidateData.PresentAddress.Address));
            parameters.Add(new MySqlParameter("PresentLandlineNo_d", candidateData.PresentAddress.LandlineNumber));
            parameters.Add(new MySqlParameter("PresentMobileNo_d", candidateData.PresentAddress.MobileNumber));
            parameters.Add(new MySqlParameter("PresentAltMobileNo_d", candidateData.PresentAddress.Alt_Mobile_Number));
            parameters.Add(new MySqlParameter("PresentCity_d", candidateData.PresentAddress.City));
            parameters.Add(new MySqlParameter("PresentState_d", candidateData.PresentAddress.State));
            parameters.Add(new MySqlParameter("PresentLandMark_d", candidateData.PresentAddress.Landmark));
            parameters.Add(new MySqlParameter("PresentPostalCode_d", candidateData.PresentAddress.PostalCode));
            parameters.Add(new MySqlParameter("PresentPoliceStationName_d", candidateData.PresentAddress.PoliceStationName));

            parameters.Add(new MySqlParameter("PermanentAddressType_d", "Permanent"));
            parameters.Add(new MySqlParameter("PermanentAddress_d", candidateData.PermanentAddress.Address));
            parameters.Add(new MySqlParameter("PermanentLandlineNo_d", candidateData.PermanentAddress.LandlineNumber));
            parameters.Add(new MySqlParameter("PermanentMobileNo_d", candidateData.PermanentAddress.MobileNumber));
            parameters.Add(new MySqlParameter("PermanentAltMobileNo_d", candidateData.PermanentAddress.Alt_Mobile_Number));
            parameters.Add(new MySqlParameter("PermanentCity_d", candidateData.PermanentAddress.City));
            parameters.Add(new MySqlParameter("PermanentState_d", candidateData.PermanentAddress.State));
            parameters.Add(new MySqlParameter("PermanentLandmark_d", candidateData.PermanentAddress.Landmark));
            parameters.Add(new MySqlParameter("PermanentPostalCode_d", candidateData.PermanentAddress.PostalCode));
            parameters.Add(new MySqlParameter("PermanentPoliceStation_d", candidateData.PermanentAddress.PoliceStationName));

            string organizationName = GetOrganizationName(Convert.ToInt32(candidateData.OrganizationId));
            parameters.Add(new MySqlParameter("OrganizationName_d", organizationName));

            if (string.IsNullOrEmpty(candidateData.CandidateId))
            {               
                parameters.Add(new MySqlParameter("TempCandidateId", 0));
                parameters.Get("TempCandidateId").Direction = System.Data.ParameterDirection.Output;
            }
            else
            {                
                parameters.Add(new MySqlParameter("CandidateId", candidateData.CandidateId));
            }


            int candidateId = 0;
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                int i;   
                if (string.IsNullOrEmpty(candidateData.CandidateId))
                {
                    i = DataAccess.ExecuteNonQuery(conn, null, System.Data.CommandType.StoredProcedure, "InsertinCandidateRegistration", parameters);
                    candidateId = Convert.ToInt32(parameters.Get("TempCandidateId").Value.ToString());
                }
                else
                {
                    i = DataAccess.ExecuteNonQuery(conn, null, System.Data.CommandType.StoredProcedure, "UpdatingCandidateRegistration", parameters);
                    candidateId = Convert.ToInt32(candidateData.CandidateId);
                }

                if (candidateId != 0)
                {
                    using (MySqlConnection selConnection = new MySqlConnection(connectionString))
                    {
                        selConnection.Open();
                        string sqlString = "delete FROM candidate_emp_history where CANDIDATE_ID = " + candidateId + "";
                        DataAccess.ExecuteNonQuery(selConnection, sqlString);
                    }

                    for (int j = 0; j < candidateData.employeeHistory.Length; j++)
                    {
                        EmployerHistoryRequestDTO employerHistory = candidateData.employeeHistory[j];
                        Parameters employersHistoryParameters = new Parameters();
                        employersHistoryParameters.Add(new MySqlParameter("Candidateid_d", candidateId));
                        employersHistoryParameters.Add(new MySqlParameter("EmployerName_d", employerHistory.EmployerName));
                        employersHistoryParameters.Add(new MySqlParameter("EmployeeId_d", employerHistory.EmployeeId));
                        employersHistoryParameters.Add(new MySqlParameter("FromDate_d", employerHistory.FromDate));
                        employersHistoryParameters.Add(new MySqlParameter("ToDate_d", employerHistory.ToDate));
                        employersHistoryParameters.Add(new MySqlParameter("StreetAddress_d", employerHistory.StreetAddress));
                        employersHistoryParameters.Add(new MySqlParameter("EmployersLandlineNo_d", employerHistory.EmployerLandlineNo));
                        employersHistoryParameters.Add(new MySqlParameter("City_d", employerHistory.City));
                        employersHistoryParameters.Add(new MySqlParameter("State_d", employerHistory.State));
                        employersHistoryParameters.Add(new MySqlParameter("Country_d", employerHistory.Country));
                        employersHistoryParameters.Add(new MySqlParameter("PostalCode_d", employerHistory.PostalCode));

                        employersHistoryParameters.Add(new MySqlParameter("JobTitle_d", employerHistory.JobTitle));
                        employersHistoryParameters.Add(new MySqlParameter("ReasonForLeaving_d", employerHistory.ReasonForLeaving));
                        int empStatus;
                        if (employerHistory.EmploymentType == "Full Time")
                        {
                            empStatus = 1;
                        }
                        else if (employerHistory.EmploymentType == "Contractor")
                        {
                            empStatus = 0;
                        }
                        else
                        {
                            empStatus = 1;
                        }
                        employersHistoryParameters.Add(new MySqlParameter("EmploymentType_d", empStatus));
                        employersHistoryParameters.Add(new MySqlParameter("OADName_d", employerHistory.OAD_Name));
                        employersHistoryParameters.Add(new MySqlParameter("OADAddress_d", employerHistory.OAD_Address));

                        employersHistoryParameters.Add(new MySqlParameter("OADLandlineNo_d", employerHistory.OAD_LandlineNo));
                        employersHistoryParameters.Add(new MySqlParameter("SupervisorName_d", employerHistory.SupervisorName));
                        employersHistoryParameters.Add(new MySqlParameter("SupervisorTitle_d", employerHistory.SupervisorTitle));
                        employersHistoryParameters.Add(new MySqlParameter("CTC_d", employerHistory.CTC));
                        employersHistoryParameters.Add(new MySqlParameter("SupervisorLandlineNo_d", employerHistory.SupervisorTitle));

                        employersHistoryParameters.Add(new MySqlParameter("SupervisorEmailAddress_d", employerHistory.SupervisorEmailAddress));
                        employersHistoryParameters.Add(new MySqlParameter("HRName_d", employerHistory.HR_Name));
                        employersHistoryParameters.Add(new MySqlParameter("HRTitle_d", employerHistory.HR_Title));
                        employersHistoryParameters.Add(new MySqlParameter("HRLandlineNo_d", employerHistory.HR_LandlineNo));
                        employersHistoryParameters.Add(new MySqlParameter("HREmailAddress_d", employerHistory.HR_EmailAddress));

                        employersHistoryParameters.Add(new MySqlParameter("RepManagerName_d", employerHistory.Rep_Manager_Name));
                        employersHistoryParameters.Add(new MySqlParameter("RepManagerCompanyName_d", employerHistory.Rep_Manager_Company_Name));
                        employersHistoryParameters.Add(new MySqlParameter("RepManagerTitle_d", employerHistory.Rep_Manager_Title));
                        employersHistoryParameters.Add(new MySqlParameter("RepManagerMobileNo_d", employerHistory.Rep_Manager_Mobile_No));
                        employersHistoryParameters.Add(new MySqlParameter("RepManagerEmailID_d", employerHistory.Rep_Manager_EmailID));
                        int k;
                        if (string.IsNullOrEmpty(candidateData.CandidateId))
                        {
                            k = DataAccess.ExecuteNonQuery(conn, null, System.Data.CommandType.StoredProcedure, "InsertInEmployersHistory", employersHistoryParameters);
                        }
                        else
                        {
                            k = DataAccess.ExecuteNonQuery(conn, null, System.Data.CommandType.StoredProcedure, "UpdateInEmployersHistory", employersHistoryParameters);
                        }
                    }
                }
                return candidateId;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string GetInsuffDates(int _CandidateID, out string _InSuffClearedDate)
        {
            string _InSuffRaisedDate = String.Empty; _InSuffClearedDate = "";
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            using (MySqlConnection selConnection = new MySqlConnection(GetConnectionString()))
            {
                string sqlString = "select * from candidate_registration where CANDIDATE_ID="+_CandidateID+"";

                DataTable dtCandidateRegistrationTable = DataAccess.GetDataTable(selConnection, sqlString);

                if (dtCandidateRegistrationTable.Rows.Count > 0)
                {
                    _InSuffRaisedDate = dtCandidateRegistrationTable.Rows[0]["Insuff_Raise_Date"].ToString();
                    _InSuffRaisedDate = DateTime.ParseExact(_InSuffRaisedDate, "dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture).ToString("dd-MM-yyyy");
                    _InSuffClearedDate = dtCandidateRegistrationTable.Rows[0]["Insuff_Cleared_Date"].ToString();
                    _InSuffClearedDate = DateTime.ParseExact(_InSuffClearedDate, "dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture).ToString("dd-MM-yyyy");
                }
            }
            return _InSuffRaisedDate;
        }

        public string GetCandidateStatus(int _candiadateID, int _organizationID)
        {
            string _candidateStatusID = null;
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            using (MySqlConnection selConnection = new MySqlConnection(GetConnectionString()))
            {
                string sqlString = "select Status from Candidate_Status where Status_Id = (" +
                                            "select Status_Id" +
                                            " from candidate_registration" +
                                            " where CANDIDATE_ID = "+_candiadateID+" AND ORGANIZATION_ID = "+ _organizationID + ")";


                DataTable dtCandidateRegistrationTable = DataAccess.GetDataTable(selConnection, sqlString);

                if (dtCandidateRegistrationTable.Rows.Count > 0)
                {
                    _candidateStatusID = dtCandidateRegistrationTable.Rows[0]["Status"].ToString();
                }

            }
            return _candidateStatusID;
        }

        public string GetOrganizationName(int orgId)
        {
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string ConnectionString = GetConnectionString();
            MySqlConnection conn = null;
            string organizationName = string.Empty;
            try
            {
                conn = new MySqlConnection(ConnectionString);
                conn.Open();
                string sqlString = null;
                sqlString = "Select organization_name from organizations where Organization_id=" + orgId + ";";
                DataTable dtCandidateData = DataAccess.GetDataTable(conn, sqlString);
                {
                    organizationName = dtCandidateData.Rows[0]["ORGANIZATION_NAME"].ToString();
                }
                return organizationName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

        public string GetFileReferenceId(int candidateId)
        {
            string fileRefId = null;
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();

            using (MySqlConnection selConnection = new MySqlConnection(GetConnectionString()))
            {
                string sqlString = "select FILE_REF_ID from file_storage where CANDIDATE_ID = " + candidateId + "";

                DataTable dtFileStorageData = DataAccess.GetDataTable(selConnection, sqlString);

                if (dtFileStorageData.Rows.Count > 0)
                {
                    fileRefId = dtFileStorageData.Rows[0]["FILE_REF_ID"].ToString();
                }

            }
            return fileRefId;
        }

        public int FindNumberOfRecords(int candidateId, string tableName)
        {
            int noOfRows = 0;
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string ConnectionString = GetConnectionString();
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(ConnectionString);
                conn.Open();
                string sqlString = "";
                if (tableName.ToUpper().Equals("CANDIDATE_REGISTRATION"))
                {
                    sqlString = "select * from " + tableName + " where candidate_id=" + candidateId + ";";
                }
                else
                {
                    sqlString = "select * from " + tableName + " where FILE_REF_ID=" + candidateId + ";";
                }
                DataTable dtCandidateData = DataAccess.GetDataTable(conn, sqlString);
                noOfRows = dtCandidateData.Rows.Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return noOfRows;
        }

        public bool IsValidUser(string userName, string password)
        {
            try
            {
                string sqlString = "select count(*) from login lg where lg.UserName = '" + userName + "' and lg.password = '" + password + "' ;";
                DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
                string ConnectionString = GetConnectionString();
                MySqlConnection conn = null;
                try
                {
                    conn = new MySqlConnection(ConnectionString);
                    conn.Open();
                    long usercount = (long)DataAccess.GetScalar(conn, sqlString);
                    return usercount > 0;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SubmitCandidateDocuments(string method, int candidateId, FileStorageDTO _fileStorageDTO)
        {
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null;
            int m;
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                Parameters fileStorageParameters = new Parameters();
                fileStorageParameters.Add(new MySqlParameter("Candidateid_d", candidateId));
                fileStorageParameters.Add(new MySqlParameter("PancardExt_d", _fileStorageDTO.Pancard_EXT));
                fileStorageParameters.Add(new MySqlParameter("Pancard_d", _fileStorageDTO.Pancard));
                fileStorageParameters.Add(new MySqlParameter("AdharcardExt_d", _fileStorageDTO.Adharcard_EXT));
                fileStorageParameters.Add(new MySqlParameter("Adharcard_d", _fileStorageDTO.Adharcard));
                fileStorageParameters.Add(new MySqlParameter("RationcardExt_d", _fileStorageDTO.Rationcard_EXT));
                fileStorageParameters.Add(new MySqlParameter("Rationcard_d", _fileStorageDTO.Rationcard));
                fileStorageParameters.Add(new MySqlParameter("VotercardExt_d", _fileStorageDTO.Votercard_EXT));
                fileStorageParameters.Add(new MySqlParameter("Votercard_d", _fileStorageDTO.Votercard));
                fileStorageParameters.Add(new MySqlParameter("PassportExt_d", _fileStorageDTO.Passport_EXT));
                fileStorageParameters.Add(new MySqlParameter("Passport_d", _fileStorageDTO.Passport));
                fileStorageParameters.Add(new MySqlParameter("DrivingLicenceExt_d", _fileStorageDTO.DrivingLicence_EXT));
                fileStorageParameters.Add(new MySqlParameter("DrivingLicence_d", _fileStorageDTO.DrivingLicence));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificateExt1_d", _fileStorageDTO.AcademicCertificate1_EXT));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificate1_d", _fileStorageDTO.AcademicCertificate1));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificateExt2_d", _fileStorageDTO.AcademicCertificate2_EXT));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificate2_d", _fileStorageDTO.AcademicCertificate2));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificateExt3_d", _fileStorageDTO.AcademicCertificate3_EXT));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificate3_d", _fileStorageDTO.AcademicCertificate3));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificateExt4_d", _fileStorageDTO.AcademicCertificate4_EXT));
                fileStorageParameters.Add(new MySqlParameter("AcademicCertificate4_d", _fileStorageDTO.AcademicCertificate4));
                fileStorageParameters.Add(new MySqlParameter("CandidateRelievingLetterExt1_d", _fileStorageDTO.RelievingLetter1_EXT));
                fileStorageParameters.Add(new MySqlParameter("CandidateReleivingLetter1_d", _fileStorageDTO.RelievingLetter1));
                fileStorageParameters.Add(new MySqlParameter("CandidateRelievingLetterExt2_d", _fileStorageDTO.RelievingLetter2_EXT));
                fileStorageParameters.Add(new MySqlParameter("CandidateReleivingLetter2_d", _fileStorageDTO.RelievingLetter2));
                fileStorageParameters.Add(new MySqlParameter("CandidateRelievingLetterExt3_d", _fileStorageDTO.RelievingLetter3_EXT));
                fileStorageParameters.Add(new MySqlParameter("CandidateReleivingLetter3_d", _fileStorageDTO.RelievingLetter3));
                fileStorageParameters.Add(new MySqlParameter("CandidateSalarySlipExt1_d", _fileStorageDTO.SalarySlip1_EXT));
                fileStorageParameters.Add(new MySqlParameter("CandidateSalarySlip1_d", _fileStorageDTO.SalarySlip1));
                fileStorageParameters.Add(new MySqlParameter("CandidateSalarySlipExt2_d", _fileStorageDTO.SalarySlip2_EXT));
                fileStorageParameters.Add(new MySqlParameter("CandidateSalarySlip2_d", _fileStorageDTO.SalarySlip2));
                fileStorageParameters.Add(new MySqlParameter("CandidateSalarySlipExt3_d", _fileStorageDTO.SalarySlip3_EXT));
                fileStorageParameters.Add(new MySqlParameter("CandidateSalarySlip3_d", _fileStorageDTO.SalarySlip3));
                fileStorageParameters.Add(new MySqlParameter("SignatureExt_d", _fileStorageDTO.Signature_EXT));
                fileStorageParameters.Add(new MySqlParameter("Signature_d", _fileStorageDTO.Signature));

                if (method.Equals("POST"))
                {
                    m = DataAccess.ExecuteNonQuery(conn, null, System.Data.CommandType.StoredProcedure, "InsertintoFileStoreage", fileStorageParameters);
                }
                else
                {
                    m = DataAccess.ExecuteNonQuery(conn, null, System.Data.CommandType.StoredProcedure, "UpdateintoFileStoreage", fileStorageParameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return m;
        }


        public int DeleteCandidateRecord(int candidateID, string organizationID)
        {
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null;
            Parameters parameters = new Parameters();
            parameters.Add(new MySqlParameter("CandidateID", candidateID));
            int i = 0;

            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                i = DataAccess.ExecuteNonQuery(conn, null, System.Data.CommandType.StoredProcedure, "DeleteCandidate", parameters);
                return i;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }            
        }


        public FileStorageDTO GetFilesForExistingCandidate(int candidateID, FileStorageDTO fileStorageData)
        {
            DataAccess.DataAccess DataAccess = new DataAccess.DataAccess();
            string connectionString = GetConnectionString();
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                string sqlString = null;
                sqlString = "select * from file_storage where CANDIDATE_ID = " + candidateID + ";";
                DataTable dtFileStorageData = DataAccess.GetDataTable(conn, sqlString);
                if (dtFileStorageData.Rows.Count > 0)
                {
                    if (String.IsNullOrEmpty(fileStorageData.Pancard_EXT))
                    {
                        fileStorageData.Pancard_EXT = (dtFileStorageData.Rows[0]["PAN_CARD_EXT"] != null) ? dtFileStorageData.Rows[0]["PAN_CARD_EXT"].ToString() : null;
                        fileStorageData.Pancard = (dtFileStorageData.Rows[0]["PAN_CARD"] != null) ? (byte[])dtFileStorageData.Rows[0]["PAN_CARD"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.Adharcard_EXT))
                    {
                        fileStorageData.Adharcard_EXT = (dtFileStorageData.Rows[0]["ADHARCARD_EXT"] != null) ? dtFileStorageData.Rows[0]["ADHARCARD_EXT"].ToString() : null;
                        fileStorageData.Adharcard = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["ADHARCARD"])) ? (byte[])dtFileStorageData.Rows[0]["ADHARCARD"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.Rationcard_EXT))
                    {
                        fileStorageData.Rationcard_EXT = (dtFileStorageData.Rows[0]["RATION_CARD_EXT"] != null) ? dtFileStorageData.Rows[0]["RATION_CARD_EXT"].ToString() : null;
                        fileStorageData.Rationcard = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["RATION_CARD"])) ? (byte[])dtFileStorageData.Rows[0]["RATION_CARD"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.Votercard_EXT))
                    {
                        fileStorageData.Votercard_EXT = (dtFileStorageData.Rows[0]["VOTERCARD_EXT"] != null) ? dtFileStorageData.Rows[0]["VOTERCARD_EXT"].ToString() : null;
                        fileStorageData.Votercard = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["VOTERCARD"])) ? (byte[])dtFileStorageData.Rows[0]["VOTERCARD"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.Passport_EXT))
                    {
                        fileStorageData.Passport_EXT = (dtFileStorageData.Rows[0]["PASSPORT_EXT"] != null) ? dtFileStorageData.Rows[0]["PASSPORT_EXT"].ToString() : null;
                        fileStorageData.Passport = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["PASSPORT"])) ? (byte[])dtFileStorageData.Rows[0]["PASSPORT"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.DrivingLicence_EXT))
                    {
                        fileStorageData.DrivingLicence_EXT = (dtFileStorageData.Rows[0]["DRIVING_LICENCE_EXT"] != null) ? dtFileStorageData.Rows[0]["DRIVING_LICENCE_EXT"].ToString() : null;
                        fileStorageData.DrivingLicence = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["DRIVING_LICENCE"])) ? (byte[])dtFileStorageData.Rows[0]["DRIVING_LICENCE"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.AcademicCertificate1_EXT))
                    {
                        fileStorageData.AcademicCertificate1_EXT = (dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE1_EXT"] != null) ? dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE1_EXT"].ToString() : null;
                        fileStorageData.AcademicCertificate1 = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE1"])) ? (byte[])dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE1"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.AcademicCertificate2_EXT))
                    {
                        fileStorageData.AcademicCertificate2_EXT = (dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE2_EXT"] != null) ? dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE2_EXT"].ToString() : null;
                        fileStorageData.AcademicCertificate2 = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE2"])) ? (byte[])dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE2"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.AcademicCertificate3_EXT))
                    {
                        fileStorageData.AcademicCertificate3_EXT = (dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE3_EXT"] != null) ? dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE3_EXT"].ToString() : null;
                        fileStorageData.AcademicCertificate3 = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE3"])) ? (byte[])dtFileStorageData.Rows[0]["ACADEMIC_CERTIFICATE3"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.Signature_EXT))
                    {
                        fileStorageData.Signature_EXT = (dtFileStorageData.Rows[0]["SIGNATURE_EXT"] != null) ? dtFileStorageData.Rows[0]["SIGNATURE_EXT"].ToString() : null;
                        fileStorageData.Signature = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["SIGNATURE"])) ? (byte[])dtFileStorageData.Rows[0]["SIGNATURE"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.RelievingLetter1_EXT))
                    {
                        fileStorageData.RelievingLetter1_EXT = (dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER1_EXT"] != null) ? dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER1_EXT"].ToString() : null;
                        fileStorageData.RelievingLetter1 = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER1"])) ? (byte[])dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER1"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.RelievingLetter2_EXT))
                    {
                        fileStorageData.RelievingLetter2_EXT = (dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER2_EXT"] != null) ? dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER2_EXT"].ToString() : null;
                        fileStorageData.RelievingLetter2 = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER2"])) ? (byte[])dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER2"] : null;
                    }

                    if (String.IsNullOrEmpty(fileStorageData.RelievingLetter3_EXT))
                    {
                        fileStorageData.RelievingLetter3_EXT = (dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER3_EXT"] != null) ? dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER3_EXT"].ToString() : null;
                        fileStorageData.RelievingLetter3 = (!DBNull.Value.Equals(dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER3"])) ? (byte[])dtFileStorageData.Rows[0]["CANDIDATE_RELIEVING_LETTER3"] : null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
            return fileStorageData;
        }


        public void Dispose()
        {
            
        }
    }
}