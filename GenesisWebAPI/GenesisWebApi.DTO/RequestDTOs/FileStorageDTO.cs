﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesisWebApi.DTO.RequestDTOs
{
    public class FileStorageDTO
    {
        public string Pancard_EXT { get; set; }
        public byte[] Pancard { get; set; }
        public string Adharcard_EXT { get; set; }
        public byte[] Adharcard { get; set; }
        public string Rationcard_EXT { get; set; }
        public byte[] Rationcard { get; set; }
        public string Votercard_EXT { get; set; }
        public byte[] Votercard { get; set; }
        public string Passport_EXT { get; set; }
        public byte[] Passport { get; set; }
        public string DrivingLicence_EXT { get; set; }
        public byte[] DrivingLicence { get; set; }
        public string AcademicCertificate1_EXT { get; set; }
        public byte[] AcademicCertificate1 { get; set; }
        public string AcademicCertificate2_EXT { get; set; }
        public byte[] AcademicCertificate2 { get; set; }
        public string AcademicCertificate3_EXT { get; set; }
        public byte[] AcademicCertificate3 { get; set; }
        public string AcademicCertificate4_EXT { get; set; }
        public byte[] AcademicCertificate4 { get; set; }
        public string RelievingLetter1_EXT { get; set; }
        public byte[] RelievingLetter1 { get; set; }

        public string SalarySlip1_EXT { get; set; }
        public byte[] SalarySlip1 { get; set; }
        public string RelievingLetter2_EXT { get; set; }
        public byte[] RelievingLetter2 { get; set; }
        public string SalarySlip2_EXT { get; set; }
        public byte[] SalarySlip2 { get; set; }
        public string RelievingLetter3_EXT { get; set; }
        public byte[] RelievingLetter3 { get; set; }
        public string SalarySlip3_EXT { get; set; }
        public byte[] SalarySlip3 { get; set; }
        public string Signature_EXT { get; set; }
        public byte[] Signature { get; set; }
    }
}
