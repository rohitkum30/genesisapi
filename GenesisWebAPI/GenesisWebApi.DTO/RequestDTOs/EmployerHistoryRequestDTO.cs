﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesisWebApi.DTO.RequestDTOs
{
    public class EmployerHistoryRequestDTO
    {       
        public string EmployerName { get; set; }
        public string EmployeeId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string StreetAddress { get; set; }
        public string EmployerLandlineNo { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string JobTitle { get; set; }
        public string ReasonForLeaving { get; set; }
        public string EmploymentType { get; set; }
        public string OAD_Name { get; set; }
        public string OAD_Address { get; set; }
        public string OAD_LandlineNo { get; set; }
        public string SupervisorName { get; set; }
        public string SupervisorTitle { get; set; }
        public string CTC { get; set; }
        public string SupervisorLandlineNo { get; set; }
        public string SupervisorEmailAddress { get; set; }
        public string HR_Name { get; set; }
        public string HR_Title { get; set; }
        public string HR_LandlineNo { get; set; }
        public string HR_EmailAddress { get; set; }
        public string Rep_Manager_Name { get; set; }
        public string Rep_Manager_Company_Name { get; set; }
        public string Rep_Manager_Title { get; set; }
        public string Rep_Manager_Mobile_No { get; set; }
        public string Rep_Manager_EmailID { get; set; }
    }
}
