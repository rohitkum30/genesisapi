﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesisWebApi.DTO.RequestDTOs
{
    public class CandidateRequestDTO
    {
        public string CandidateId { get; set; }
        public string OrganizationId { get; set; }
        public string Name { get; set; }
        public string FathersName { get; set; }
        public string SurName { get; set; }
        public string SchoolOrCollegeName1 { get; set; }
        public string DateOfEntering1 { get; set; }
        public string DateOfLeaving1 { get; set; }
        public string ExamPassed1 { get; set; }
        public string SchoolOrCollegeName2 { get; set; }
        public string DateOfEntering2 { get; set; }
        public string DateOfLeaving2 { get; set; }
        public string ExamPassed2 { get; set; }
        public string SchoolOrCollegeName3 { get; set; }
        public string DateOfEntering3 { get; set; }
        public string DateOfLeaving3 { get; set; }
        public string ExamPassed3 { get; set; }
        public string SchoolOrCollegeName4 { get; set; }
        public string DateOfEntering4 { get; set; }
        public string DateOfLeaving4 { get; set; }
        public string ExamPassed4 { get; set; }
        public bool Pending_Criminal_Case { get; set; }
        public bool Pending_Educational_Case { get; set; }
        public string Pending_Case_Detail { get; set; }
        public string PassportNumber { get; set; }
        public string PassportIssueDate { get; set; }
        public string PassportPlaceOfIssue { get; set; }
        public string PassportIssuingAuthority { get; set; }
        public string ValidUpto { get; set; }
        public string SubmissionPlace { get; set; }        
        public AddressRequestDTO PresentAddress { get; set; }
        public AddressRequestDTO PermanentAddress { get; set; }
        public EmployerHistoryRequestDTO[] employeeHistory { get; set; }
    }
}
