﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenesisWebApi.DTO.RequestDTOs
{
    public class AddressRequestDTO
    {      
        
        public string Address { get; set; }
        public string LandlineNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Alt_Mobile_Number { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Landmark { get; set; }
        public string PostalCode { get; set; }
        public string PoliceStationName { get; set; }
    }
}
