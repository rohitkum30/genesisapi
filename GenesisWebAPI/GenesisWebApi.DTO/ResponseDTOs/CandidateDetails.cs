﻿namespace GenesisWebApi.DTO.ResponseDTOs
{
    /// <summary>
    /// Response Structure
    /// </summary>
    public class CandidateDetails
    {
        public string CandidateName { get; set; }
        public string FathersName { get; set; }
        public string SurName { get; set; }
        public string SchoolOrCollegeName1 { get; set; }
        public string DateOfEntering1 { get; set; }
        public string DateOfLeaving1 { get; set; }
        public string ExamPassed1 { get; set; }
        public string SchoolOrCollegeName2 { get; set; }
        public string DateOfEntering2 { get; set; }
    }
}

